import java.util.HashSet;

public class Turma {
    private Disciplina disciplina;
    private int id;
    private Professor p;
    private HashSet<Aluno> alunos;

    public Turma(Disciplina disciplina, int id) {
        this.disciplina = disciplina;
        this.id = id;
        alunos = disciplina.getAlunos();
    }

    public int getId(){
        return id;
    }

    public void setProfessor(Professor p){
        this.p = p;
    }
    public String getAlunos() {
        return alunos.toString();
    }

    public String getProfessor() {
        return p.getNome();
    }

}