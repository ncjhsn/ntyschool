public class Aluno extends Pessoa {

    private static int horasMax = 28;
    private int horas;

    public Aluno(String n, int id) {
        super(n, id);
    }

    public int getHorasMax() {
        return horasMax;
    }

    public void addHoras(int h) {
        int total = horas + h;
        if (total > horasMax) {
            System.out.println("Deu Bret Cupinxa");
        } else {
            horas += h;
        }
    }

    @Override
    public String toString() {
        return "[Nome]: " + getNome();
    }


}