import java.util.HashMap;

import java.util.Scanner;



public class Professor extends Pessoa {

    private Scanner in = new Scanner(System.in);
    private int cargaHoraria;
    private int horasMax = 12;
    private HashMap<Disciplina, Turma> aulasLecionadas = new HashMap<>();
    public Professor(String n, int id) {
        super(n, id);
        cargaHoraria = 0;
    }

    public void addTurma(Disciplina d, Turma t) {
        if (cargaHoraria + d.getCargaHoraria() <= horasMax) {
            if (!aulasLecionadas.containsKey(d)) {
                aulasLecionadas.put(d, t);
                cargaHoraria += d.getCargaHoraria();
            } else {
                System.out.println("Este professor ja ministra esta disciplina em outra turma, deseja altera-la ? [s] [n]");
                String resp = in.nextLine();
                if (resp.equals("s")) {
                    aulasLecionadas.put(d, t);
                } else {
                    System.out.println();
                }
            }
        }
    }

    public void removeDisciplina(Disciplina d){
        if(aulasLecionadas.containsKey(d)){
            cargaHoraria -= d.getCargaHoraria();
            aulasLecionadas.remove(d);
        }
    }

    public void getTurmas() {
        if(aulasLecionadas.size() > 0) {
            aulasLecionadas.forEach((d, t) -> System.out.println("Disciplina: " + d.getDescricao() + ", Turma: " + t.getId()));
        }else{
            System.out.println("Este professor nao tem nenhuma turma");
        }
    }

    public int getHorasMax() {
        return horasMax;
    }

    public int getCargaHoraria() {
        return cargaHoraria;
    }

    @Override
    public String toString() {
        return "[Nome]: " + getNome();
    }

}